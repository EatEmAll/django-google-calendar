from __future__ import print_function
import httplib2
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from oauth2client import file, client, tools
import datetime as dt

SCOPES = 'https://www.googleapis.com/auth/calendar'
scopes = [SCOPES]
APPLICATION_NAME = 'Google Calendar API Python'


class google_calendar_api:
    def __init__(self, client_secret_path, token_path):
        self.client_secret_file_path = client_secret_path
        self.token_path = token_path
        self.service = self.build_service()

    def build_service(self):
        store = file.Storage(self.token_path)
        creds = store.get()
        if not creds or creds.invalid:
            flow = client.flow_from_clientsecrets(self.client_secret_file_path, SCOPES)
            creds = tools.run_flow(flow, store)
        service = build('calendar', 'v3', http=creds.authorize(httplib2.Http()))
        return service

    def create_event(self, calendar_id, start, end, summary, description='',
                     timestamp_dict_kwargs=None, **event_kwargs):
        timestamp_dict_kwargs = timestamp_dict_kwargs or {}
        body = {
            'summary': summary,
            'description': description,
            'start': self.get_timestamp_dict(start, **timestamp_dict_kwargs),
            'end': self.get_timestamp_dict(end, **timestamp_dict_kwargs),
        }
        body.update(event_kwargs)
        event = self.service.events().insert(calendarId=calendar_id, body=body).execute()
        return event['id']

    def update_event(self, calendar_id, event_id, start, end, summary, description='',
                     timestamp_dict_kwargs=None, **event_kwargs):
        timestamp_dict_kwargs = timestamp_dict_kwargs or {}
        start = self.get_timestamp_dict(start, **timestamp_dict_kwargs)
        end = self.get_timestamp_dict(end, **timestamp_dict_kwargs)
        try:
            event = self.service.events().get(calendarId=calendar_id, eventId=event_id).execute()
            event["start"] = start
            event["end"] = end
            event["summary"] = summary
            event["description"] = description
            event.update(event_kwargs)
            updated_event = self.service.events().update(calendarId=calendar_id,
                                                         eventId=event['id'],
                                                         body=event).execute()
            return updated_event["id"]
        except HttpError as e:
            if e.resp.status == 404:
                return self.create_event(calendar_id, start, end, summary, description,
                                         timestamp_dict_kwargs, **event_kwargs)
            else:
                raise e

    def get_event(self, calendar_id, event_id):
        return self.service.events().get(calendarId=calendar_id, eventId=event_id).execute()

    def delete_event(self, calendar_id, event_id):
        return self.service.events().delete(calendarId=calendar_id, eventId=event_id).execute()

    @staticmethod
    def get_timestamp_dict(timestamp, timezone='UTC', timestamp_type='datetime'):
        dt_dict = {}
        if isinstance(timestamp, dt.datetime):
            timestamp_type = 'dateTime'
            # remove units smaller than a minute
            timestamp = dt.datetime(timestamp.year,
                                    timestamp.month,
                                    timestamp.day,
                                    timestamp.hour,
                                    timestamp.minute).isoformat()
        elif isinstance(timestamp, dt.date):
            timestamp_type = 'date'
            timestamp = timestamp.isoformat()
        elif not isinstance(timestamp, str):
            raise TypeError('unsupported datetime type %s' % type(timestamp))
        dt_dict[timestamp_type] = timestamp
        dt_dict['timeZone'] = timezone
        return dt_dict
